after clone the project please follow steps bellow:
    1. cp .env.example .env
    2. configure your environtment in .env file
    3. npm install
    4. npx sequelize db:create
    5. npx sequelize db:migrate
    6. npm run start
