const Hapi = require('hapi');
require('dotenv').config();
const axios = require('axios');
const _ = require('lodash');
const models = require('./models');
const inert = require('inert'); // serve static content
const decode = require('decode-html');
const Joi = require('joi');

const parser = require('fast-xml-parser');

const {
  handleFileUpload,
  download,
  getImage,
  deleteImage
} = require('./utils.js');

const port = process.env.PORT || 3000;
const openapikey = process.env.ELEVENIA_API_KEY || '';
const productDetailUrl = `http://api.elevenia.co.id/rest/prodservices/product/details/`;
const productListingUrl = `http://api.elevenia.co.id/rest/prodservices/product/listing`;

//28022696 example product id

const server = Hapi.server({
  port,
  host: 'localhost',
  routes: {
    files: {
      relativeTo: __dirname
    },
    cors: true
  }
});

server.route({
  method: 'GET',
  path: '/',
  handler: (request, h) => {
    return { message: 'Welcome to the beginning of nothingness' };
  }
});

server.route({
  method: 'GET',
  path: '/{productUrl}',
  handler: async (request, h) => {
    return h.file(request.params.productUrl);
  }
});
server.route({
  method: 'GET',
  path: '/upload/{productUrl}',
  handler: async (request, h) => {
    return h.file(`upload/${request.params.productUrl}`);
  }
});

server.route({
  method: 'GET',
  path: '/products',
  handler: async (req, h) => {
    let { pageSize = 100, page = 1 } = req.query;
    page = page - 1;
    if (page < 0) page = 0;
    let products = [];
    let result = {
      count: 0,
      rows: {}
    };
    try {
      const result = await models.product.findAndCountAll({
        limit: pageSize,
        offset: page * pageSize
      });
      products = result.rows.map((item) => {
        const product = item;

        product.image = getImage(req.info.host, product.image);

        return product;
      });
    } catch (err) {
      console.log(err);
      return err;
    }
    return {
      products,
      meta: {
        total: result.count,
        page: page + 1,
        pageSize,
        totalPage: Math.ceil(result.count / pageSize)
      },
      message: 'success get data'
    };
  }
});

server.route({
  method: 'GET',
  path: '/product/{productId}',
  handler: async (req, h) => {
    const { productId } = req.params;
    let product;
    try {
      product = await models.product.findByPk(productId);
      if (!product) {
        return {
          statusCode: 404,
          error: 'Not found',
          message: 'Product not found'
        };
      }
      product.image = getImage(req.info.host, product.image);
    } catch (err) {
      console.log(err);
      return err;
    }
    return { message: 'success get data', product };
  }
});

server.route({
  path: '/update/{productId}',
  method: 'POST',
  options: {
    payload: {
      output: 'stream',
      allow: 'multipart/form-data'
    }
  },
  handler: async (req, reply) => {
    const { productId } = req.params;
    const joiValidation = {
      productId: Joi.required(),
      description: Joi.string().required(),
      name: Joi.string().min(3).required(),
      price: Joi.string().min(5).required(),
      sku: Joi.string().min(5).required()
    };

    const res = await Joi.validate(
      { ...req.payload, productId },
      joiValidation,
      {
        allowUnknown: true
      },
      (err, value) => {
        if (err) {
          return {
            statusCode: 422,
            error: err.details[0].message,
            message: 'Invalid request data'
          };
        } else {
          return { error: null };
        }
      }
    );
    if (res.error) {
      return res;
    }
    const { file, description, name, price } = req.payload;
    let { sku } = req.payload;
    const product = await models.product.findByPk(productId);
    if (!product) {
      return {
        statusCode: 404,
        error: 'Not found',
        message: 'Product not found'
      };
    }
    let image = product.image;
    sku = sku.replace(/[^\w\s]/gi, '');
    sku = sku.replace(' ', '_');
    try {
      if (file) image = await handleFileUpload(file, productId);
      await product.update({ description, name, sku, image, price });
      product.image = getImage(req.info.host, product.image);
    } catch (err) {
      console.log(err);
      return err;
    }
    return { message: 'success update data', product };
  }
});

server.route({
  path: '/delete/{productId}',
  method: 'POST',
  handler: async (req, h) => {
    const { productId } = req.params;
    const product = await models.product.findByPk(productId);
    if (!product) {
      return {
        statusCode: 404,
        error: 'Not found',
        message: 'Product not found'
      };
    }
    try {
      const productImage = product.image;
      await product.destroy();
      if (!productImage.includes('no-image')) {
        deleteImage(productImage);
      }
    } catch (err) {
      console.log(err);
      return err;
    }
    return { message: 'success delete data' };
  }
});

server.route({
  method: 'GET',
  path: '/elevenia',
  handler: async (request, h) => {
    let data = '';
    let products = [];
    let transaction;
    let selectedProduct = null;

    let image = '';
    await axios
      .get(productListingUrl, {
        headers: {
          'Content-type': 'application/xml',
          'Accept-Charset': 'utf-8',
          openapikey
        }
      })
      .then((response) => {
        data = parser.parse(response.data);
        products = _.get(data, 'Products.product', []);
      })
      .catch((error) => {
        console.log('error 3 ' + error);
        return { error };
      });
    if (products.length) {
      try {
        transaction = await models.sequelize.transaction();
        for (const product of products) {
          // eslint-disable-next-line no-await-in-loop
          await axios
            .get(`${productDetailUrl}${product.prdNo}`, {
              headers: {
                'Content-type': 'application/xml',
                'Accept-Charset': 'utf-8',
                openapikey
              }
            })
            .then(async (response) => {
              selectedProduct = parser.parse(response.data);
              selectedProduct = _.get(selectedProduct, 'Product', null);
            })
            .catch((error) => {
              console.error('error 3 ' + error);
              return { error };
            });

          if (selectedProduct) {
            const splitUrl = selectedProduct.prdImage01.split('.');
            const mime = splitUrl[splitUrl.length - 1];
            if (selectedProduct.prdImage01.includes('no_image')) {
              image = 'no-image.png';
            } else {
              image = `upload/${selectedProduct.prdNo}.${mime}`;
              // eslint-disable-next-line no-await-in-loop
              await download(selectedProduct.prdImage01, image, function () {
                console.log(`success get image`);
              });
            }
            // eslint-disable-next-line no-await-in-loop
            await models.product.upsert(
              {
                id: selectedProduct.prdNo,
                sku: selectedProduct.sellerPrdCd,
                name: selectedProduct.prdNm,
                description: decode(selectedProduct.htmlDetail),
                price: selectedProduct.selPrc,
                image
              },
              { transaction }
            );
          }
        }
        await transaction.commit();
      } catch (err) {
        if (transaction) await transaction.rollback();
        console.error(err);
        return err;
      }
    }
    return {
      message: 'Success get data'
    };
  }
});

const init = async () => {
  await server.register(inert);
  await server.start();
  console.log('Server running on %s', port);
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

init();
