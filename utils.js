const request = require('request');
const fs = require('fs');

const handleFileUpload = (file, name) => {
  return new Promise((resolve, reject) => {
    const filename = file.hapi.filename.split('.');
    const mime = filename[filename.length - 1];
    const data = file._data;
    fs.writeFile(`./upload/${name}.${mime}`, data, (err) => {
      if (err) {
        reject(err);
      }
      resolve(`upload/${name}.${mime}`);
    });
  });
};

const download = (uri, filename, callback) => {
  request.head(uri, (err, res, body) => {
    console.log('content-type:', res.headers['content-type']);
    console.log('content-length:', res.headers['content-length']);

    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};

const getImage = (host, relativePath, defaultPath = 'no-image.png') => {
  if (fs.existsSync(relativePath)) {
    return `${host}/${relativePath}`;
  } else {
    return `${host}/${defaultPath}`;
  }
};

const deleteImage = (relativePath) => {
  if (fs.existsSync(relativePath)) {
    fs.unlinkSync(relativePath);
  }
};

module.exports = {
  handleFileUpload,
  download,
  getImage,
  deleteImage
};
